import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReusableFormComponent } from './reusable-form/reusable-form.component';
import{MatTableModule} from '@angular/material/table'
import{MatPaginatorModule} from '@angular/material/paginator' 
import{   MatButtonModule} from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatFormFieldModule, MatLabel } from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import { HttpClientModule } from '@angular/common/http';
import {MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS} from '@angular/material/snack-bar';
@NgModule({
  declarations: [AppComponent, ReusableFormComponent],
  imports: [BrowserModule, AppRoutingModule, FormsModule, ReactiveFormsModule,MatPaginatorModule,
    MatTableModule,
    MatButtonModule,
    BrowserAnimationsModule,
  MatSelectModule,
FormsModule,
ReactiveFormsModule,
MatFormFieldModule,
MatCardModule,
HttpClientModule,
MatSnackBarModule],

  providers: [ { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: {duration: 2000}}],
  bootstrap: [AppComponent],
})
export class AppModule { }
