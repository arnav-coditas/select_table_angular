
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { HttpService } from './service/http.service';
import { MatSnackBar } from '@angular/material/snack-bar';
interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
 
  objectKeys = Object.keys;
  
  foods: any = [
    {value: 'steak-0', viewValue: 'Steak'},
    {value: 'pizza-1', viewValue: 'Pizza'},
    {value: 'tacos-2', viewValue: 'Tacos'},
  ];

  buttons = ['Edit','Delete'];
  

  
  tableData: any;
  dataSource!:any;

  

  ngOnInit() {
   this.getData()
 
  }

  
 form!:FormGroup
 foodControl =new FormControl('', [Validators.required]);
  constructor(
    public router: Router,
    private http: HttpService,
    private formBuilder:FormBuilder,
    private snakbar: MatSnackBar
   
  ) {

    this.form = new FormGroup({
      food: this.foodControl,
  
    });
  }

  buttonEvent!:any
  handleClick(event:any, eventType:any) {
    console.log(event, eventType);
  }
  

  submitted=true;

onSubmit(){
  this.submitted = true;
  if(!this.foodControl.value) {
    this.snakbar.open("Invalid Login Credentials", "Try again!", {
     
      panelClass: ['red-snackbar'],
      });
     
    
  } else {
    console.log(this.foodControl.value)
  }
}




  
   











  getData(){
    this.http.getDetails().subscribe((res: any) => {
      this.dataSource = res;
      console.log(res);
    });
  }

  selectedFood2!:string
  columnHeader:any = {
    id: '  ID',
    name: 'Name',
    username:'Username',
    email:'Email',
   phone:'Phone',
   actions: 'Actions',
    
  };


  onFoodSelection2() {
    console.log('Writer changed...');
    console.log(this.selectedFood2);
  
  }

}







  



