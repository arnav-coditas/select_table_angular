import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reusable-form',
  templateUrl: './reusable-form.component.html',
  styleUrls: ['./reusable-form.component.scss']
})
export class ReusableFormComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
